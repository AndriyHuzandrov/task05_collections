package com.nexu6.task05_collections.bintree.servicepack;

public class MenuTxtConst {
  static final int FIRST_MENU_ITEM = 0;
  public static final String WRONG_MENU_ITEM_MSG = "Wrong menu item";
  public static final String INPUT_ERROR = "Error while reading input";
  static final String WLC_MSG = "Utility menu to test binary tree container\n";
  public static final String EXIT_MSG = "Make choice or 0 to exit the program : ";
  public static final String PAUSE_MSG = "Hit ENTER key to continue ";
  static final String FILL_TREE_MSG = " or AUTO to fill the tree container" +
                                        " with 10 random elements: ";
  static final String INSERT_NODE_MSG = "Enter key:value to add to the tree";
  static final String FIND_MSG = "Enter key to get value: ";
  public static final String BAD_ARG = "Bad argument";
  static final String NO_KEY_MSG = "No such key\n";
  static final String DEL_KEY_MSG = "Enter a key for an entry to be deleted: ";
  public static String INP_STRING_MSG = "Input string to add to the queue: ";

  private MenuTxtConst() {}
}

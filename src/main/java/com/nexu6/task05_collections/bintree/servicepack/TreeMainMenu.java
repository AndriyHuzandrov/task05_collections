package com.nexu6.task05_collections.bintree.servicepack;

import com.nexu6.task05_collections.bintree.model.BinaryTree;
import com.nexu6.task05_collections.bintree.view.SingleEntryStOutMonitor;
import com.nexu6.task05_collections.bintree.view.StOutMonitor;
import com.nexu6.task05_collections.bintree.view.Updatable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.apache.logging.log4j.Logger;

public class TreeMainMenu {
  private BufferedReader txtInput;
  private static Logger mainLogger;
  private BinaryTree dataSource;
  private Map<Integer, String> txtMenu;
  private Map<Integer, Displayable> execMenu;

  public TreeMainMenu(Logger logger, BinaryTree ds) {
    txtInput = new BufferedReader(new InputStreamReader(System.in));
    dataSource = ds;
    mainLogger = logger;
    txtMenu = new LinkedHashMap<>();
    execMenu = new LinkedHashMap<>();
    txtMenu.put(1, "Insert data in the tree");
    txtMenu.put(2, "Retrieve data from the tree");
    txtMenu.put(3, "Delete data from the tree");
    txtMenu.put(4, "Show tree");
    txtMenu.put(5, "Get statistics(Beta)");
    execMenu.put(0, this::exitApp);
    execMenu.put(1, this::insertVal);
    execMenu.put(2, this::retrieveVal);
    execMenu.put(3, this::removeVal);
    execMenu.put(4, this::showTree);
  }
  private void showTree() {
    proceedMenuExec(new StOutMonitor(mainLogger));
  }
  private void exitApp() {
    System.exit(0);
  }
  private void insertVal() {
    mainLogger.info(MenuTxtConst.INSERT_NODE_MSG  + MenuTxtConst.FILL_TREE_MSG);
    BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
    try {
      String inp = br.readLine();
      if (inp.compareTo("AUTO") == 0) {
        Random rnd = new Random();
          for (int i = 0; i < 11; i++) {
            dataSource.put(rnd.nextInt(100), rnd.nextInt(1000));
          }
      } else if(inp.matches("\\d+:\\d+")) {
        String[] inpElements = inp.split(":");
        Integer key = Integer.valueOf(inpElements[0]);
        Integer val = Integer.valueOf(inpElements[1]);
        mainLogger.info("Inserting node with key = " + key + " and value = " + val + "\n");
        dataSource.put(key, val);
        } else {
        throw new IllegalArgumentException();
      }
    } catch(IOException e){
     mainLogger.error(MenuTxtConst.INPUT_ERROR);
    }
    catch (IllegalArgumentException e) {
      mainLogger.warn(MenuTxtConst.BAD_ARG);
      insertVal();
    }
    proceedMenuExec(new StOutMonitor(mainLogger));
  }


  private void retrieveVal(){
    mainLogger.info(MenuTxtConst.FIND_MSG);
    Integer key = getArg();
    if(dataSource.containsKey(key)) {
      proceedMenuExec(new SingleEntryStOutMonitor(mainLogger,
                      dataSource.get(key).toString()));
    } else {
      proceedMenuExec(new SingleEntryStOutMonitor(mainLogger, MenuTxtConst.NO_KEY_MSG));
    }
  }
  private void removeVal() {
    mainLogger.info(MenuTxtConst.DEL_KEY_MSG);
    Integer key = getArg();
    if(dataSource.containsKey(key)) {
      dataSource.remove(key);
      proceedMenuExec(new StOutMonitor(mainLogger));
    } else {
      proceedMenuExec(new SingleEntryStOutMonitor(mainLogger, MenuTxtConst.NO_KEY_MSG));
    }
  }

  private void proceedMenuExec(Updatable monitor) {
    dataSource.subscribeObserver(monitor);
    dataSource.notifyObservers();
    showPause();
    dataSource.removeObserver(monitor);
    getChoice();
  }

  private void showMenu() {
    Set<Map.Entry<Integer, String>> menuContext = txtMenu.entrySet();
    mainLogger.info("\n");
    mainLogger.info(MenuTxtConst.WLC_MSG);
    for(Map.Entry<Integer, String> menuElement : menuContext) {
      mainLogger.info(String.format("%-3d%s%n", menuElement.getKey(), menuElement.getValue()));
    }
    mainLogger.info(MenuTxtConst.EXIT_MSG);
  }

  public void getChoice() {
    showMenu();
    try {
      String input = txtInput.readLine();
      if (!input.matches("\\d")) {
        throw new IllegalArgumentException();
      } else if (Integer.valueOf(input) < MenuTxtConst.FIRST_MENU_ITEM
          || Integer.valueOf(input) > txtMenu.size()) {
        throw new IllegalArgumentException();
      } else {
        execMenu.get(Integer.valueOf(input)).display();
      }
    }
    catch (IOException e) {
      mainLogger.error(MenuTxtConst.INPUT_ERROR);
    }
    catch (IllegalArgumentException exc) {
      mainLogger.info("\n");
      mainLogger.warn(MenuTxtConst.WRONG_MENU_ITEM_MSG);
      getChoice();
    }
  }
  private void showPause() {
    mainLogger.info(MenuTxtConst.PAUSE_MSG);
    try {
      txtInput.readLine();
    } catch (IOException e) {
      mainLogger.error(MenuTxtConst.INPUT_ERROR);
    }
  }
  private Integer getArg() {
    String input;
    try {
      input = txtInput.readLine();
      if (!input.matches("\\d+")) {
        throw new IllegalArgumentException();
      } else {
        return Integer.valueOf(input);
      }
    } catch (IOException e) {
      mainLogger.error(MenuTxtConst.INPUT_ERROR);
    }
    catch (IllegalArgumentException exc) {
      mainLogger.info("\n");
      mainLogger.warn(MenuTxtConst.BAD_ARG);
      getArg();
    }
    return -1;
  }
}
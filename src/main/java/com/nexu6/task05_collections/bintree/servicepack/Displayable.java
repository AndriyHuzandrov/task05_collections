package com.nexu6.task05_collections.bintree.servicepack;

@FunctionalInterface
public interface Displayable {
  void display();
}
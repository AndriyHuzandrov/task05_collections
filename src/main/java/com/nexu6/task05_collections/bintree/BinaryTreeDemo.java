package com.nexu6.task05_collections.bintree;

import com.nexu6.task05_collections.bintree.model.BinaryTree;
import com.nexu6.task05_collections.bintree.servicepack.TreeMainMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BinaryTreeDemo {
  private static final Logger mainLogger = LogManager.getLogger(BinaryTreeDemo.class);

  public static void main(String[] args) {
    TreeMainMenu menu = new TreeMainMenu(mainLogger, new BinaryTree<Integer, Integer>());
    menu.getChoice();
  }
}

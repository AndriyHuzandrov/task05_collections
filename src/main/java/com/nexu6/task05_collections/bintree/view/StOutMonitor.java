package com.nexu6.task05_collections.bintree.view;

import com.nexu6.task05_collections.bintree.model.Publishable;
import org.apache.logging.log4j.Logger;

public class StOutMonitor implements Updatable {
  static Logger appLogger;

  public StOutMonitor(Logger l) {
    appLogger = l;
  }
  public void update(Publishable dataTree) {
    appLogger.info(dataTree.print());
  }
}

package com.nexu6.task05_collections.bintree.view;

import com.nexu6.task05_collections.bintree.model.Publishable;
import org.apache.logging.log4j.Logger;

public class SingleEntryStOutMonitor extends StOutMonitor {
  private String valToShow;

  public SingleEntryStOutMonitor(Logger l, String v) {
    super(l);
    valToShow = v;
  }
  public void update(Publishable dataTree) {
    appLogger.info(valToShow);
  }
}

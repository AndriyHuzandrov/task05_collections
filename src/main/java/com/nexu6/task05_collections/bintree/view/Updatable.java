package com.nexu6.task05_collections.bintree.view;

import com.nexu6.task05_collections.bintree.model.Publishable;

public interface Updatable<T extends Publishable> {
  void update(T dataSource);
}

package com.nexu6.task05_collections.bintree.view;

import com.nexu6.task05_collections.bintree.model.Publishable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileMonitor implements Updatable {
  private static final Logger logStat = LogManager.getLogger(FileMonitor.class);
  static  {
    logStat.info("some statistics");
  }

  public void update(Publishable dataTree) {}

}

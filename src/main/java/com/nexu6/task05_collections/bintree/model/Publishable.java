package com.nexu6.task05_collections.bintree.model;

import com.nexu6.task05_collections.bintree.view.Updatable;

public interface Publishable {
  void subscribeObserver(Updatable obs);
  void removeObserver(Updatable obs);
  void notifyObservers();
  String print();

}

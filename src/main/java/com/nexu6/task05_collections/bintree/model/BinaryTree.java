package com.nexu6.task05_collections.bintree.model;

import com.nexu6.task05_collections.bintree.view.Updatable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BinaryTree<K extends Comparable<K>, V> implements Publishable, Map {
  private String printOut;
  private int sizeCounter;
  private List<Updatable> observers;
  private Node rootNode;
  public class Node {
    private K key;
    private V data;
    private Node leftChild;
    private Node rightChild;

    private Node(K key, V data) {
      this.key = key;
      this.data = data;
    }
    @Override
    public String toString() {
      return String.format("Node %s%5s%n", key.toString(), data.toString());
    }
  }
   public BinaryTree() {
    observers = new ArrayList<>();
    rootNode = null;
    sizeCounter = 0;
  }
  public void subscribeObserver(Updatable obs) {
    observers.add(obs);
  }
  public void removeObserver(Updatable obs) {
    if(observers.indexOf(obs) >= 0) {
      observers.remove(obs);
    }
  }
  @SuppressWarnings("unchecked")
  public void notifyObservers() {
    for(Updatable obs : observers) {
      obs.update(this);
    }
  }
  private Node findNode(K key) {
    Node currentNode;
    if(isEmpty()) {
      return null;
    } else {
      currentNode = rootNode;
      while(currentNode != null && currentNode.key != key) {
        if(key.compareTo(currentNode.key) < 0) {
          currentNode = currentNode.leftChild;
        } else if(key.compareTo(currentNode.key) > 0) {
          currentNode = currentNode.rightChild;
        }
      }
    }
    return currentNode;
  }

  private Node addNode(Node currentNode, Node nodeToAdd, K key) {
    if(currentNode == null) {
      return nodeToAdd;
    }
    if (key.compareTo(currentNode.key) < 0) {
      currentNode.leftChild = addNode(currentNode.leftChild, nodeToAdd, key);
    } else if (key.compareTo(currentNode.key) > 0) {
      currentNode.rightChild = addNode(currentNode.rightChild, nodeToAdd, key);
    }
    return currentNode;
  }
  public String print() {
    printOut = "";
    traverseInOrder(rootNode);
    return printOut;
  }
  private void traverseInOrder(Node node) {
    if (node != null) {
      sizeCounter++;
      traverseInOrder(node.leftChild);
      printOut += node.toString();
      traverseInOrder(node.rightChild);
    }
  }
  @Override
  public int size() {
    sizeCounter = 0;
    traverseInOrder(rootNode);
    return sizeCounter;
  }
  @Override
  public boolean isEmpty() {
    return rootNode == null;
  }
  @Override
  public boolean containsKey(Object key) {
    K k = (K) key;
    return findNode(k) != null;
  }
  @Override
  public boolean containsValue(Object value) {
    return false;
  }
  @Override
  public Object get(Object key) {
    K k = (K) key;
    return findNode(k);
  }
  @Override
  public Object put(Object key, Object value) {
    K k = (K) key;
    V v = (V) value;
    Node newNode = new Node(k, v);
    if(isEmpty()) {
      rootNode = newNode;
      return rootNode;
    } else {
      return addNode(rootNode, newNode, k);
    }
  }
  @Override
  public Object remove(Object key) {
    K k = (K) key;
    System.out.println("Key passed" + k);

    return deleteNode(rootNode, k);
  }
  private Node deleteNode(Node curNode, K delKey) {
    if(curNode == null)
      return null;
    if(delKey.compareTo(curNode.key) ==0){
      System.out.println("Key accepted" + delKey);
      if(curNode.leftChild == null && curNode.rightChild == null) {
        return null;
      } else if(!(curNode.leftChild != null && curNode.rightChild != null)) {
          return curNode.rightChild == null ? curNode.leftChild : curNode.rightChild;
      } else {
         K smallestKey = findMin(curNode.rightChild);
         curNode.key = smallestKey;
         curNode.rightChild = deleteNode(curNode.rightChild, smallestKey);
         return curNode;
      }
    }
    if(delKey.compareTo(curNode.key) < 0){
      curNode.leftChild = deleteNode(curNode.leftChild, delKey);
      return curNode;
    }
    curNode.rightChild = deleteNode(curNode.rightChild, delKey);
    return curNode;
  }
  private K findMin(Node curNode) {
    return curNode.leftChild == null ? curNode.key : findMin(curNode.leftChild);
  }
  @Override
  public void putAll(Map m) {
    Set<Map.Entry<K,V>> dataSet = m.entrySet();
    for(Map.Entry<K,V> data : dataSet) {
      put(data.getKey(), data.getValue());
    }
  }
  @Override
  public void clear() {
    rootNode = null;
  }
  @Override
  public Set keySet() {
    return null;
  }
  @Override
  public Collection values() {
    return null;
  }
  @Override
  public Set<Entry> entrySet() {
    return null;
  }
}

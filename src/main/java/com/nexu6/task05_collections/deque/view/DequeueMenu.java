package com.nexu6.task05_collections.deque.view;

import com.nexu6.task05_collections.bintree.servicepack.Displayable;
import com.nexu6.task05_collections.bintree.servicepack.MenuTxtConst;
import com.nexu6.task05_collections.deque.control.Controller;
import com.nexu6.task05_collections.deque.control.Performable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.logging.log4j.Logger;
public class DequeueMenu {
  private static Performable controlUnit;
  private static BufferedReader txtInput;
  private static Logger mainLogger;

  private enum menuItems {
    ITEM_0("Exit program", DequeueMenu::exitApp),
    ITEM_1( "Add element to the head of the queue", DequeueMenu::addHeadElement),
    ITEM_2( "Add element to the end of the queue", DequeueMenu::addTailElement),
    ITEM_3( "Get first element of the queue", DequeueMenu::getHeadElement),
    ITEM_4( "Get last element of the queue", DequeueMenu::getTailElement),
    ITEM_5( "Delete firs element", DequeueMenu::remHeadElement),
    ITEM_6( "Delete last element", DequeueMenu::remTailElement),
    ITEM_7("Print queue", DequeueMenu::printQueue);
    String name;
    Displayable action;

    menuItems(String name, Displayable action) {
      this.name = name;
      this.action = action;
    }
  }

  public DequeueMenu(Logger log) {
    controlUnit = new Controller<String>();
    mainLogger = log;
    txtInput = new BufferedReader(new InputStreamReader(System.in));
  }

  private static void showMenu() {
    for(menuItems item : menuItems.values()) {
      if(item.ordinal() == 0) {
        continue;
      }
      mainLogger.info(String.format("%-5d%s%n", item.ordinal(), item.name));
    }
    mainLogger.info(MenuTxtConst.EXIT_MSG);
  }
  public static void getChoice() {
    showMenu();
    try {
      String input = txtInput.readLine();
      if (!input.matches("\\d")) {
        throw new IllegalArgumentException();
      } else if (Integer.valueOf(input) < menuItems.ITEM_0.ordinal()
          || Integer.valueOf(input) > menuItems.values().length) {
        throw new IllegalArgumentException();
      } else {
        for(menuItems item : menuItems.values()){
          if(item.ordinal() == Integer.valueOf(input)){
            item.action.display();
          }
        }
      }
    }
    catch (IOException e) {
      mainLogger.error(MenuTxtConst.INPUT_ERROR);
    }
    catch (IllegalArgumentException exc) {
      mainLogger.info("\n");
      mainLogger.warn(MenuTxtConst.WRONG_MENU_ITEM_MSG);
      getChoice();
    }
  }
  private static String getArg() {
    String input;
    try {
      input = txtInput.readLine();
      if (input.isEmpty()) {
        throw new IllegalArgumentException();
      } else {
        return input;
      }
    } catch (IOException e) {
      mainLogger.error(MenuTxtConst.INPUT_ERROR);
    }
    catch (IllegalArgumentException exc) {
      mainLogger.warn(MenuTxtConst.BAD_ARG);
      getChoice();
    }
    return "";
  }
  private static void exitApp() {
    System.exit(0);
  }
  private static void addHeadElement() {
   mainLogger.info(MenuTxtConst.INP_STRING_MSG);
   String dataToAdd = getArg();
   controlUnit.addData(dataToAdd, 0);
   getChoice();
  }
  private static void addTailElement() {
    mainLogger.info(MenuTxtConst.INP_STRING_MSG);
    String dataToAdd = getArg();
    controlUnit.addData(dataToAdd, 1);
    getChoice();
  }
  private static void getHeadElement() {
   mainLogger.info("First element > " + controlUnit.retriveData(0) + "\n");
   getChoice();
  }
  private static void getTailElement() {
   mainLogger.info("Last element > " + controlUnit.retriveData(1) + "\n");
   getChoice();
  }
  private static void printQueue() {
   mainLogger.info(controlUnit.print());
   showPause();
   getChoice();
  }
  private static void remHeadElement() {
   controlUnit.remData(0);
   getChoice();
  }
  private static void remTailElement() {
   controlUnit.remData(1);
   getChoice();
  }
  private static void showPause() {
   mainLogger.info(MenuTxtConst.PAUSE_MSG);
   try {
    txtInput.readLine();
   } catch (IOException e) {
     mainLogger.error(MenuTxtConst.INPUT_ERROR);
   }
  }
}

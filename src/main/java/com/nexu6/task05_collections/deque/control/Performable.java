package com.nexu6.task05_collections.deque.control;

public interface Performable<T> {
  void addData(T data, int addMode);
  T retriveData(int mode);
  String print();
  void remData(int mode);

}

package com.nexu6.task05_collections.deque.control;

import com.nexu6.task05_collections.deque.model.DequeContainer;
import java.util.Deque;
import java.util.function.Consumer;

public class Controller<T> implements Performable<T>{
  private Deque dataSource;

  @SuppressWarnings("unchaked")
  public Controller() {
    dataSource = new DequeContainer<>(String.class, 10);
  }
  public void addData(T insertData, int mode) {
    @SuppressWarnings("unchaked")
    Consumer<T> insertion = mode == 0 ? s -> dataSource.offerFirst(s) :
                                        s -> dataSource.offerLast(s);
    insertion.accept(insertData);
  }
  public T retriveData(int mode) {
    return mode == 0 ? (T) dataSource.peekFirst() : (T) dataSource.peekLast();
  }
  public String print() {
    String sep = " -- ";
    String nLine = "\n";
    StringBuilder outStr = new StringBuilder();
    for(int i = 0; i < dataSource.toArray().length; i++) {
      outStr.append(i);
      outStr.append(sep);
      outStr.append(dataSource.toArray()[i]);
      outStr.append(nLine);
    }
    return outStr.toString();
  }
  public void remData(int mode) {
    @SuppressWarnings("unchaked")
    Consumer<Integer> insertion = mode == 0 ? s -> dataSource.pollFirst() :
        s -> dataSource.pollLast();
    insertion.accept(mode);
  }
}

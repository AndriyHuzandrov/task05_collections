package com.nexu6.task05_collections.deque.model;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.function.BiFunction;

public class DequeContainer<T> implements Deque {
  private int dequeMaxCap;
  private BiFunction<Class<T>, Integer, T[]> createNewArr;
  private T[] dataArr;
  private Class<T> dequeType;

  public DequeContainer(Class<T> cl, int maxCap) {
    dequeMaxCap = maxCap;
    dequeType = cl;
    createNewArr = (arrClass, arrSize) -> (T[]) Array.newInstance(arrClass, arrSize);
    @SuppressWarnings("unchaked")
    final T[] dataArr = createNewArr.apply(dequeType, 0);
    this.dataArr = dataArr;
  }
  private boolean addToEmptyArr(Object o) {
      dataArr = createNewArr.apply(dequeType, 1);
      dataArr[0] = (T) o;
      return true;
  }
  @Override
  public boolean offerFirst(Object o) {
    if(dataArr.length == 0) {
      return addToEmptyArr(o);
    } else if(dataArr.length == dequeMaxCap) {
      return false;
    } else {
      T[] tempArr = dataArr;
      dataArr = createNewArr.apply(dequeType, tempArr.length + 1);
      dataArr[0] = (T) o;
      System.arraycopy(tempArr, 0, dataArr, 1, tempArr.length);
      return true;
    }
  }
  @Override
  public boolean offerLast(Object o) {
    if(dataArr.length == 0) {
      return addToEmptyArr(o);
    } else if(dataArr.length == dequeMaxCap) {
      return false;
    } else {
      T[] tempArr = dataArr;
      dataArr = createNewArr.apply(dequeType, tempArr.length + 1);
      System.arraycopy(tempArr, 0, dataArr, 0, tempArr.length);
      dataArr[tempArr.length] = (T) o;
      return true;
    }
  }
  @Override
  public Object pollFirst() {
    T[] tempArr;
    if(dataArr.length == 0) {
      return null;
    } else {
      tempArr = dataArr;
      dataArr = createNewArr.apply(dequeType, (tempArr.length - 1));
      System.arraycopy(tempArr, 1, dataArr, 0, dataArr.length);
     return dataArr[0];
    }
  }
  @Override
  public Object pollLast() {
    T[] tempArr;
    if(dataArr.length == 0) {
      return null;
    } else {
      tempArr = dataArr;
      dataArr = createNewArr.apply(dequeType, (tempArr.length - 1));
      System.arraycopy(tempArr, 0, dataArr, 0, dataArr.length - 1);
      return dataArr[0];
    }
  }
  @Override
  public Object getFirst() {

    return dataArr[0];
  }
  @Override
  public Object getLast() {
    return dataArr[dataArr.length - 1];
  }
  @Override
  public Object peekFirst() {
    return dataArr[0] == null ? null : dataArr[0];
  }
  @Override
  public Object peekLast() {
    return dataArr[dataArr.length - 1] == null ? null : dataArr[dataArr.length - 1];
  }
  @Override
  public int size() {
    return dataArr.length;
  }
  @Override
  public boolean isEmpty() {
    return dataArr.length == 0;
  }


  @Override
  public Iterator iterator() {
    return new Iterator() {
      private int currInd = 0;
      @Override
      public boolean hasNext() {
        return currInd < dataArr.length;
      }
      @Override
      public Object next() {
        return dataArr[currInd++];
      }
    };
  }

  @Override
  public Object[] toArray() {
    return dataArr;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

  @Override
  public Iterator descendingIterator() {
    return null;
  }
  @Override
  public void addFirst(Object o) {}

  @Override
  public void addLast(Object o) {

  }
  @Override
  public Object removeFirst() {
    return null;
  }

  @Override
  public Object removeLast() {
    return null;
  }
  @Override
  public boolean removeFirstOccurrence(Object o) {
    return false;
  }
  @Override
  public boolean removeLastOccurrence(Object o) {
    return false;
  }
  @Override
  public boolean add(Object o) {
    return false;
  }
  @Override
  public boolean offer(Object o) {
    return false;
  }
  @Override
  public Object remove() {
    return null;
  }
  @Override
  public Object poll() {
    return null;
  }
  @Override
  public Object element() {
    return null;
  }
  @Override
  public Object peek() {
    return null;
  }
  @Override
  public boolean addAll(Collection c) {
    return false;
  }
  @Override
  public void clear() {
  }
  @Override
  public boolean retainAll(Collection c) {
    return false;
  }
  @Override
  public boolean removeAll(Collection c) {
    return false;
  }
  @Override
  public void push(Object o) {
  }
  @Override
  public Object pop() {
    return null;
  }
  @Override
  public boolean remove(Object o) {
    return false;
  }
  @Override
  public boolean containsAll(Collection c) {
    return false;
  }
  @Override
  public boolean contains(Object o) {
    return false;
  }
}

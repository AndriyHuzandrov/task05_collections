package com.nexu6.task05_collections.deque;

import com.nexu6.task05_collections.bintree.BinaryTreeDemo;
import com.nexu6.task05_collections.deque.view.DequeueMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeQueueDemo {
  private static final Logger mainLogger = LogManager.getLogger(BinaryTreeDemo.class);
  public static void main(String[] args) {
    DequeueMenu titleView = new DequeueMenu(mainLogger);
    titleView.getChoice();
  }
}
